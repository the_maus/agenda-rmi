package br.edu.ifba.bsi.sd.agenda.servidor;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import br.edu.ifba.bsi.sd.agenda.bean.PhonebookEntry;

public class PhonebookServer implements IPhonebookServer {
	
	private List<PhonebookEntry> phonebook = new ArrayList<PhonebookEntry>();

	@Override
	public List<PhonebookEntry> getPhonebook() throws RemoteException {
		return phonebook;
	}
	
	@Override
	public boolean existeEntry(Integer id) throws RemoteException {
		boolean existe = false;
		
		ListIterator<PhonebookEntry> litr = phonebook.listIterator();
		while(litr.hasNext()){
			PhonebookEntry contato = litr.next();
			if(contato.getId().equals(id)){
				existe = true;
			}
		}
		return existe;
	}	
	

	@Override
	public synchronized void addEntry(PhonebookEntry entry) throws RemoteException {
		ListIterator<PhonebookEntry> litr = phonebook.listIterator();
		while(litr.hasNext()){
			PhonebookEntry contato = litr.next();
			if(contato.getTelefone().equals(entry.getTelefone())){
				throw new RuntimeException("TELEFONE_EXISTENTE");
			}
		}
		
		entry.setId(phonebook.size()+1);
		phonebook.add(entry);

	}

	@Override
	public synchronized void modifyEntry(PhonebookEntry entry) throws RemoteException {
		ListIterator<PhonebookEntry> list = phonebook.listIterator();
		while(list.hasNext()){
			PhonebookEntry contato = list.next();
			if(contato.getTelefone().equals(entry.getTelefone()) && !contato.getId().equals(entry.getId())){
				throw new RuntimeException("TELEFONE_EXISTENTE");
			}
		}
		
		
		ListIterator<PhonebookEntry> litr = phonebook.listIterator();
		while(litr.hasNext()){
			PhonebookEntry contato = litr.next();
			if(contato.getId().equals(entry.getId())){
				litr.set(entry);
				break;
			}
		}
		
		
	}

	@Override
	public synchronized void deleteEntry(PhonebookEntry entry) throws RemoteException {
		if(!existeEntry(entry.getId())){
			throw new RuntimeException("REGISTRO_INEXISTENTE");
		}
		
		ListIterator<PhonebookEntry> litr = phonebook.listIterator();
		while(litr.hasNext()){
			PhonebookEntry contato = litr.next();
			if(contato.getId().equals(entry.getId())){
				litr.remove();
				break;
			}
		}
	}
	
	public static void main(String args[]) {
        
        try {
            
//        	System.setProperty("java.rmi.server.codebase", IPhonebookServer.class.getProtectionDomain().getCodeSource().getLocation().toString());
        	
        	PhonebookServer obj = new PhonebookServer();
            
            
            IPhonebookServer stub = (IPhonebookServer) UnicastRemoteObject.exportObject(obj, 0);

            // Registra o objeto do servidor para que ele possa fornecer os servi�os
            Registry registry = LocateRegistry.getRegistry();
            registry.bind("PhonebookServer", stub);

            System.err.println("Servidor pronto.");
        } catch (Exception e) {
            System.err.println("Erro no servidor: " + e.toString());
            e.printStackTrace();
        }
    }



}
