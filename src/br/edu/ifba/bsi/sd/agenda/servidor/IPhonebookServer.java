package br.edu.ifba.bsi.sd.agenda.servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import br.edu.ifba.bsi.sd.agenda.bean.PhonebookEntry;

public interface IPhonebookServer extends Remote{
	
	public List<PhonebookEntry> getPhonebook() throws RemoteException;
	public void addEntry(PhonebookEntry entry) throws RemoteException;
	public void modifyEntry(PhonebookEntry entry) throws RemoteException;
	public void deleteEntry(PhonebookEntry entry) throws RemoteException;
	public boolean existeEntry(Integer id) throws RemoteException;

}
