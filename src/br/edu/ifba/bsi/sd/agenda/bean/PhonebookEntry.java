package br.edu.ifba.bsi.sd.agenda.bean;

import java.io.Serializable;

import br.com.linkcom.neo.validation.annotation.Required;


public class PhonebookEntry implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nome, sobrenome, telefone;
	
	public Integer getId() {
		return id;
	}
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public String getSobrenome() {
		return sobrenome;
	}
	@Required
	public String getTelefone() {
		return telefone;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	
	
}
