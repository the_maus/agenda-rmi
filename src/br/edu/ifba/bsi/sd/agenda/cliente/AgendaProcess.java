package br.edu.ifba.bsi.sd.agenda.cliente;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.edu.ifba.bsi.sd.agenda.bean.PhonebookEntry;
import br.edu.ifba.bsi.sd.agenda.servidor.IPhonebookServer;

@Controller(path="/agenda/process/Agenda")
public class AgendaProcess extends MultiActionController {
	
	private static final String HOST = "localhost";
	
	@DefaultAction
	public ModelAndView listagem(WebRequestContext request){
		NeoWeb.getRequestContext();
		
        try {
            Registry registry = LocateRegistry.getRegistry(HOST);
            IPhonebookServer stub = (IPhonebookServer) registry.lookup("PhonebookServer");
            List<PhonebookEntry> listaContatos = stub.getPhonebook();
            request.setAttribute("listaContatos", listaContatos);
        } catch (Exception e) {
            request.addError("Erro ao acessar servidor: " + e.toString());
        }
        
        return new ModelAndView("process/agenda");
	}
	
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, PhonebookEntry bean){
		if(bean.getId()!=null){ //se for edi��o
			try {
	            Registry registry = LocateRegistry.getRegistry(HOST);
	            IPhonebookServer stub = (IPhonebookServer) registry.lookup("PhonebookServer");
	            boolean existe = stub.existeEntry(bean.getId());
	            if(!existe){
	            	request.addError("O registro n�o existe mais no servidor.");
	            	return listagem(request);
	            }
	        } catch (Exception e) {
	            request.addError("Erro ao acessar servidor: " + e.toString());
	        }
		}		
		return new ModelAndView("process/agendaAdd", "bean", bean);
	}
	
	
	public ModelAndView salvar(WebRequestContext request, PhonebookEntry bean){
		if(bean.getId()==null){
			try {
				Registry registry = LocateRegistry.getRegistry(HOST);
				IPhonebookServer stub = (IPhonebookServer) registry.lookup("PhonebookServer");
				stub.addEntry(bean);
				request.addMessage("Contato salvo com sucesso.");
			} catch (Exception e) {
				if(e.toString().contains("TELEFONE_EXISTENTE")){
					request.addError("J� existe um contato com este telefone.");
					return new ModelAndView("process/agendaAdd", "bean", bean);
				}else{
					request.addError("Erro ao acessar servidor: " + e.toString());
				}
			}
		}else{
			try {
				Registry registry = LocateRegistry.getRegistry(HOST);
				IPhonebookServer stub = (IPhonebookServer) registry.lookup("PhonebookServer");
				stub.modifyEntry(bean);
				request.addMessage("Contato salvo com sucesso.");
			} catch (Exception e) {
				if(e.toString().contains("TELEFONE_EXISTENTE")){
					request.addError("J� existe um contato com este telefone.");
					return new ModelAndView("process/agendaAdd", "bean", bean);
				}else{
					request.addError("Erro ao acessar servidor: " + e.toString());
				}
			}			
		}
		return listagem(request);
	}
	
	public ModelAndView excluir(WebRequestContext request, PhonebookEntry bean){
		try {
			Registry registry = LocateRegistry.getRegistry(HOST);
			IPhonebookServer stub = (IPhonebookServer) registry.lookup("PhonebookServer");
			stub.deleteEntry(bean);
			request.addMessage("Contato exclu�do com sucesso.");
		} catch (Exception e) {
			if(e.toString().contains("REGISTRO_INEXISTENTE")){
				request.addError("O registro n�o existe mais no servidor.");
			}else{
				request.addError("Erro ao acessar servidor: " + e.toString());
			}
		}
		return listagem(request);
	}
	

}
