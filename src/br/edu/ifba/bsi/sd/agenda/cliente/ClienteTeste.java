package br.edu.ifba.bsi.sd.agenda.cliente;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import br.edu.ifba.bsi.sd.agenda.servidor.IPhonebookServer;

public class ClienteTeste {
	
	 private ClienteTeste() {}

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            IPhonebookServer stub = (IPhonebookServer) registry.lookup("PhonebookServer");
            String response = String.valueOf(stub.getPhonebook().size());
            System.out.println("quantidade de contatos: " + response);
        } catch (Exception e) {
            System.err.println("Erro no cliente: " + e.toString());
            e.printStackTrace();
        }
    }

}
