<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>
<%@page import="br.edu.ifba.bsi.sd.agenda.bean.PhonebookEntry"%>

<t:tela titulo="Contato">
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:bean name="bean" valueType="<%=PhonebookEntry.class%>">
				<hr/>
				<n:panelGrid columns="1">
					<t:property name="id" type="hidden" write="false" showLabel="false" label=""/>
					<t:property name="nome" style="width: 290px;" renderAs="doubleline"/>
					<t:property name="sobrenome" style="width: 290px;" renderAs="doubleline"/>
					<t:property name="telefone" mask="telefone" renderAs="doubleline"/>
					<n:submit action="salvar" class="left btn btn-primary"><i class="fa fa-save"></i>Salvar</n:submit>
				</n:panelGrid>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:tela>

<style>
.requiredMark{
	margin-right:50px;
}

.left{
	margin-left:200px;
}

</style>