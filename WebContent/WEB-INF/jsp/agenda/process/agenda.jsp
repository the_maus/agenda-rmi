<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>
<%@page import="br.edu.ifba.bsi.sd.agenda.bean.PhonebookEntry"%>
<t:listagem titulo="Agenda">
	<t:janelaResultados >
		<n:dataGrid itens="${listaContatos}" var="contato" itemType="<%=PhonebookEntry.class%>">
			<t:property name="nome"/>
			<t:property name="sobrenome"/>
			<t:property name="telefone" mask="telefone"/>
			<n:column header="">
				<n:link action="criar" parameters="id=${contato.id}&telefone=${contato.telefone}&nome=${contato.nome}&sobrenome=${contato.sobrenome}" class="btn btn-default">
					<i class="fa fa-edit"></i>Editar
				</n:link>
				<n:link action="excluir" parameters="id=${contato.id}" class="btn btn-default">
					<i class="fa fa-trash"></i>Excluir
				</n:link>
			</n:column>
		</n:dataGrid>
	</t:janelaResultados>
</t:listagem>
<style>
#btn_excluir{
	display:none;
}
</style>